import scrapy


class PetMarketSpider(scrapy.Spider):
    name = "petmarket"
    fields = {
        'link_pagination': '//div[@class="pagination"]//a/@href',
        'link_category': '//div[@class="category-image"]/a/@href',
        'product': '//div[@class="product-grid"]/div[contains(concat(" ",normalize-space(@class)," ")," category ")]',
        'price': './/div[@class="price"]/text()',
        'old_price': './/span[@class="old_price"]/text()',
        'name': './/div[@class="description"]/address/text()',
        'img': './div[@class="image"]/a/img/@src',
        'product_link': './div[@class="image"]/a/@href'
    }
    custom_settings = {
        'CLOSESPIDER_PAGECOUNT': 0,
        'CLOSESPIDER_ITEMCOUNT': 20
    }
    start_urls = [
        'https://petmarket.ua/zootovary-dlja-sobak/suhoj-korm-dlja-sobak/royal-canin-korm-dlja-sobak/dlja-schenkov/'
        'https://petmarket.ua/zootovary-dlja-sobak',
        'https://petmarket.ua/zootovary-dlja-ryb',
        'https://petmarket.ua/zootovary-dlja-koshek/'
    ]
    allowed_domains = [
        'petmarket.ua'
    ]

    def parse(self, response):
        for product in response.xpath(self.fields["product"]):
            price = product.xpath(self.fields['price']).get()
            yield {
                'link': product.xpath(self.fields['product_link']).extract(),
                'price': price.strip() if price else product.xpath(self.fields['old_price']).get().strip(),
                'img': product.xpath(self.fields['img']).extract(),
                'name': ''.join(product.xpath(self.fields['name']).extract())
            }
        for a in response.xpath(self.fields["link_category"]):
            yield response.follow(a.extract(), callback=self.parse)
        for a in response.xpath(self.fields["link_pagination"]):
            yield response.follow(a.extract(), callback=self.parse)
